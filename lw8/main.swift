//
//  main.swift
//  lw8
//
//  Created by Daniil Lushnikov on 27.09.2021.
//  Phonebook with replaceable phone numbers

import Foundation

// Dictionary (key - contact name, value - phone)
var phonebook = [String: String]()
// n - amount of requests to add contact
let n = Int(readLine() ?? "") ?? 0

for _ in 0..<n {
    let name = readLine() ?? ""
    let phone = readLine() ?? ""
    phonebook[name] = phone;
}

for (name, phone) in phonebook {
    print(name)
    print(phone)
}

